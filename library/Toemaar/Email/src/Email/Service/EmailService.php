<?php
/**
 * Toemaar (http://code.grasvezel.nl/)
 *
 * @link      http://code.grasvezel.nl/
 * @copyright Copyright (c) 2005-2015 Toemaar. (http://www.toemaar.nl)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Toemaar\Email\Service;

use Zend\Mail\Message;
use Zend\Mime\Part;
use Zend\Mail\Transport\Smtp;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\Mail\Transport\Sendmail;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Mime;

/**
 * Email class that provides a easy templating function.
 * The email is constructed and sent of through the configured 
 * transport protocol smtp|sendmail
 * 
 * @author Jason Lentink <jason@toemaar.nl>
 *
 */
class EmailService implements ServiceManagerAwareInterface
{
    /**
     * 
     * @var ServiceLocatorInterface
     */
    protected $_serviceManager = null;
    
    /**
     * Path to the email template from the root of the project
     * 
     * @var String
     */
    protected $_template = null;
    
    /**
     * Email meta data
     * @var \stdClass
     */
    protected $_metaData = null;
    
    /**
     * I18n identifier 
     * 
     * @var string
     */
    protected $_i18n = null;
    
    /**
     * The files  that need to be attached.
     * 
     * @var string
     */
    protected $_attachments = array();
    
    /**
     * Factory constructor
     */
    public function init(){        
    }
    
    /**
     * Set the template to be used.
     * 
     * @param string $template
     * @param string $i18n
     * @throws Exception
     */
    public function setTemplate($template, $i18n){
        $this->_i18n = $i18n;
        $templatePath = null;
        
        if(is_file('data/email-templates/' . $template . '/' . $i18n . '.json'))
            $templatePath = 'data/email-templates/' . $template . '/' . $i18n;
        
        if(null == $templatePath && is_file('data/email-templates/' . $template . '/default.json'))
            $templatePath = 'data/email-templates/' . $template . '/default';
        
        if(null == $templatePath)
            throw new Exception('Template does not exists');
        
        
        $this->_template = $templatePath;
        $this->_metaData = json_decode(file_get_contents($this->_template . '.json'));   
    }
    
    public function addAttachmentByLocation($fileLocation, $fileName = null){
        if(!is_file($fileLocation) || !is_readable($fileLocation))
            throw new Exception('Attachment file is not readable');
        
        if(!$fileName)    
            $fileName = basename($fileLocation);
        
        $this->_attachments[] = array('filelocation' => $fileLocation, 'filename' => $fileName);
    }
    
    /**
     * Substitute Tags in text
     * 
     * @param string $content
     * @param tags $tags
     * @return string
     */
    protected function _substituteTags($content, $tags){
        foreach ($tags as $tag => $value) {
            $content = str_replace('{{:' . $tag . ':}}', $value, $content);
        }
        
        return $content;
    }
    
    /**
     * Create a email based on it's theme an params
     * 
     * @param array $params
     * @param string|Address\AddressInterface|array|AddressList|Traversable $address
     * @param string $name
     * @throws Exception
     * @return \Zend\Mail\Message
     */
    protected function _constructEmail($params, $address, $name){      
        
        if(null === $this->_metaData)
            throw new Exception('No template selected.');
                
        $body = new \Zend\Mime\Message();
        $parentTheme = null;
        
        if('' != $this->_metaData->ParentTheme){            
            if(is_file('data/email-templates/' . $this->_metaData->ParentTheme . '/' . $this->_i18n . '.json')){
                $parentTheme = 'data/email-templates/' . $this->_metaData->ParentTheme . '/' . $this->_i18n;
            }
            
            if(null == $parentTheme && is_file('data/email-templates/' . $this->_metaData->ParentTheme . '/default.json')){
                $parentTheme = 'data/email-templates/' . $this->_metaData->ParentTheme . '/default';
            }
        }
        
        $contentParts = array();
        
        if(is_file($this->_template . '.txt')){
            $contentText = file_get_contents($this->_template . '.txt');
        
            if($parentTheme)
                $contentText = str_replace('{{:themecontent:}}', $contentText, file_get_contents($parentTheme . '.txt'));
        
            $contentText = $this->_substituteTags($contentText, $params);
            $partText = new Part($contentText);
            $partText->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;
            $partText->type = \Zend\Mime\Mime::TYPE_TEXT;            
            $contentParts[] = $partText;
        }
        
        if(is_file($this->_template . '.html')){
            $contentHtml = file_get_contents($this->_template . '.html');
            if($parentTheme)
                $contentHtml = str_replace('{{:themecontent:}}', $contentHtml, file_get_contents($parentTheme . '.html'));
                                    
            $contentHtml = $this->_substituteTags($contentHtml, $params);
            
            $partHtml = new Part($contentHtml);
            $partHtml->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;
            $partHtml->type = \Zend\Mime\Mime::TYPE_HTML;
            $partHtml->charset = 'UTF-8';
            
            //$body->addPart($partHtml);  
            $contentParts[] = $partHtml;
        }
        
        $alternatives = new \Zend\Mime\Message();
        $alternatives->setParts($contentParts);        
        $alternativesPart = new Part($alternatives->generateMessage());
        $alternativesPart->type = "multipart/alternative;\n boundary=\"".$alternatives->getMime()->boundary()."\"";
        
        $body = new \Zend\Mime\Message();
        $body->addPart($alternativesPart);
        
        foreach($this->_attachments as $attachmentSrc){
            $attachment = new Part(fopen($attachmentSrc['filelocation'], 'r'));
            $attachment->filename = $attachmentSrc['filename'];
            $attachment->encoding = Mime::ENCODING_BASE64;
            $attachment->type = Mime::DISPOSITION_ATTACHMENT;
            $attachment->disposition = true;
            $body->addPart($attachment);
        }
        
        $message = new Message();
        $message->setSubject($this->_substituteTags($this->_metaData->Subject, $params));
        $message->setFrom($this->_metaData->From->Address, $this->_metaData->From->Name);
        $message->setReplyTo($this->_metaData->Replyto);
        $message->setBody($body);
        $message->setTo($address, $name);        
        $message->setEncoding("UTF-8");
        
        $message->setSender($this->_metaData->From->Address, $this->_metaData->From->Name);
        return $message;
    }

    /**
     * Sent a email to a user based on the set template.
     * 
     * @param array $params
     * @param string|Address\AddressInterface|array|AddressList|Traversable $address
     * @param string $name
     * @throws Exception\RuntimeException
     */
    public function send(array $params, $address, $name = null){
        $config = $this->getServiceManager()->get('Config');
        $config = $config['email-transport'];
        $message = $this->_constructEmail($params, $address, $name);

        if($config['method'] == 'smtp') {
            unset($config['method']);
            if(null == $config['connection_class'])
                unset($config['connection_class']);
              
            $options = new SmtpOptions($config);
            $transport = new Smtp();
            $transport->setOptions($options);
        } else {
            $transport = new Sendmail();
        }
        $transport->send($message);        
    }
	/* (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceManagerAwareInterface::setServiceManager()
     */
    public function setServiceManager(\Zend\ServiceManager\ServiceManager $serviceManager)
    {
        $this->_serviceManager = $serviceManager;        
    }
    
    /**
     * Grep the service manager
     * 
     * @return \Service_email\Service\ServiceLocatorInterface
     */
    public function getServiceManager(){
        return $this->_serviceManager;
    }

}