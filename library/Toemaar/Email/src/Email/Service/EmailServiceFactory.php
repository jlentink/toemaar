<?php
/**
 * Toemaar (http://code.grasvezel.nl/)
 *
 * @link      http://code.grasvezel.nl/
 * @copyright Copyright (c) 2005-2015 Toemaar. (http://www.toemaar.nl)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Toemaar\Email\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

/**
 * Service factory for email service.
 * 
 * @author Jason Lentink <jason@toemaar.nl>
 *
 */
class EmailServiceFactory implements FactoryInterface
{
    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     * @return EmailService
     */
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$service = new EmailService();
		$service->init();		
		return $service;
	}
}